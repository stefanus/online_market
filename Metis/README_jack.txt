2014-10-14
	- To make all stuff work, make sure that all foreign keys' ON DELETE be CASCADE. 
	For example, by phpMyAdmin, OnlineMarket - photos - Structure - Relation view, 
	then you can change the foreign key.
	Also, I use post_id in photos table as foreign key to posts table. It's different from
	our original design, you may need to change it.
	
	- Add index(), view(), edit() and delete() functions in PostsController. 
	Users now can see all their posts by clicking "username" -> "View my posts" button.
	And then they can view the detail or modify or delete selected post. All related
	images in /img/upload/ should also be deleted now.
	
	- To make an admin account, you need to register a normal account, then in database,
	at least change role and email to 'admin'. DO NOT change password. Since each password
	was hashed & salted before we save it.
	
	- Add show() and delete() functions in UsersController.
	Admin account now can see all users by clicking "admin" -> "Show all users" button.
	And then they can view the detail or modify or delete selected user. All related
	images in /img/upload/ should also be deleted now.
	I haven't started Suspend function. Maybe do it later.
	
	- All my work is displayed as basic & ugly pages. Help me to test and improve them.
	
	- Just a reminder, 
	Model handles its validation or relationship with other Model or some callback
	methods like beforeDelete/afterDelete...
	
	View just displays all things or generates a form for Controller, 
	
	Controller handles logic stuff, each function name correspond to its View file(.ctp)...