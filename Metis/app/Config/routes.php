<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));
    Router::connect('/metis', array('controller' => 'pages', 'action' => 'display', 'home'));

/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	Router::connect('/metis/pages/*', array('controller' => 'pages', 'action' => 'display'));
    Router::connect('/metis/users/login', array('controller' => 'users', 'action' => 'login'));
    Router::connect('/metis/users/register', array('controller' => 'users', 'action' => 'register'));
    Router::connect('/metis/users/logout', array('controller' => 'users', 'action' => 'logout'));
    Router::connect('/metis/users/update', array('controller' => 'users', 'action' => 'update'));
    Router::connect('/metis/posts/index', array('controller' => 'posts', 'action' => 'index'));
    Router::connect('/metis/posts/buy', array('controller' => 'posts', 'action' => 'buy'));
    Router::connect('/metis/posts/sell', array('controller' => 'posts', 'action' => 'sell'));
    Router::connect('/metis/users/dashboard', array('controller' => 'users', 'action' => 'dashboard'));
    Router::connect('/metis/users/myposts', array('controller' => 'users', 'action' => 'myposts'));
    Router::connect('/metis/users/show', array('controller' => 'users', 'action' => 'show'));
    /* Router::connect('/metis/users/search', array('controller' => 'users', 'action' => 'myposts')); */
    Router::connect('/metis/messages/view', array('controller' => 'messages', 'action' => 'view'));
    Router::connect('/metis/pages/about', array('controller' => 'pages', 'action' => 'about'));
/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
