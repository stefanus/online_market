<?php

class MessagesController extends AppController {

	public $helpers = array('Html', 'Form', 'Session');
	public $components = array('Session');

	// $id is post_id, we call this one when buyer want to buy a gear
	public function add($id = null) {
		$fromUserId = $this->Auth->user('user_id');
		$fromUser = $this->Auth->user('username');
		$postInfo = $this->Message->query("SELECT * FROM posts WHERE post_id = '$id';");


		$toUserId = $postInfo[0]['posts']['user_id'];
		$userInfo = $this->Message->query("SELECT * FROM users WHERE user_id = '$toUserId';");
		$toUser = $userInfo[0]['users']['username']; // $toUser = $userInfo['User']['username'];
		
		if ($this->request->is('post')) {
			$message = $this->request->data['Message']['message'];
			if (empty($message)) {
				$this->Session->setFlash(__('Please send a message.'));
				return $this->redirect($this->referer());
			}
			$link = '( http://localhost:8888/metis/posts/view/' . $id . ' )';
			$message = $message . "\n" . $link;
			$this->request->data['Message']['message'] = $message;

			$this->request->data['Message']['from_user'] = $fromUser;
			$this->request->data['Message']['to_user'] = $toUser;
			
			if ($this->Message->save($this->request->data)) {
            	// $this->Session->setFlash(__('Your message has been sent.'));
            	return $this->redirect(array('action' => 'view'));
            	// 'controller' => 'posts', 'action' => 'buy'
        	}
        	$this->Session->setFlash(__('Unable to send your message.'));
		}

        $this->set('toUser', $toUser);
	}

	public function view() {
		$currentUser = $this->Auth->user('username');
		// $allTalks contains all talks bewteen current user and others
		$allTalks = array();

		// $sendInfo contains all messages sended by currentUser.
		$sendInfo = $this->Message->find('list', array(
			'fields' => array('Message.to_user', 'Message.message', 'Message.message_id'),
			'conditions' => array('Message.from_user' => $currentUser)));
		$receiversInfo = $this->Message->find('all', array(
			'fields' => array('DISTINCT Message.to_user'),
			'conditions' => array('Message.from_user' => $currentUser)));

		foreach ($receiversInfo as $key => $value) {
			$allTalks[] = $value['Message']['to_user'];
		}

		// $receiveInfo contains all messages sended to currentUser.
		$receiveInfo = $this->Message->find('list', array(
			'fields' => array('Message.from_user', 'Message.message', 'Message.message_id'),
			'conditions' => array('Message.to_user' => $currentUser)));
		$sendersInfo = $this->Message->find('all', array(
			'fields' => array('DISTINCT Message.from_user'),
			'conditions' => array('Message.to_user' => $currentUser)));

		foreach ($sendersInfo as $key => $value) {
			$sender = $value['Message']['from_user'];
			if (!in_array($sender, $allTalks)) {
				$allTalks[] = $sender;
			}
		}

		sort($allTalks);
		$allMessages = $sendInfo + $receiveInfo;
		ksort($allMessages);
		// $allKeys keeps all message id releated with current user
		$allKeys = array_keys($allMessages);

		$this->set('currentUser', $currentUser);
		$this->set('sendInfo', $sendInfo);
		$this->set('receiveInfo', $receiveInfo);
		$this->set('allTalks', $allTalks);
		$this->set('allKeys', $allKeys);
	}

	public function reply() {
		if ($this->request->is('post')) {
			$message = $this->request->data['Message']['message'];
			if (empty($message)) {
				$this->Session->setFlash(__('Please send a message.'));
				return $this->redirect($this->referer());
			}
			if ($this->Message->save($this->request->data)) {
            	// $this->Session->setFlash(__('Your message has been sent.'));
            	return $this->redirect(array('action' => 'view'));
            	// 'controller' => 'posts', 'action' => 'buy'
        	}
        	$this->Session->setFlash(__('Unable to send your message.'));
		}
	}
}

?>