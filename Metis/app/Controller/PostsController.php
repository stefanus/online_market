<?php

class PostsController extends AppController {

	public $helpers = array('Html', 'Form', 'Session');
	public $components = array('Session');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow(array('buy', 'message', 'view'));
    }

    public function index($id = null) {
    	$userId = $this->Auth->user('user_id');

    	// If user role is admin, index page need show
    	// the user we want, passed by $id
    	if ($this->Auth->user('role') == 'admin' && !empty($id)) {
    		$userId = $id;
    	}
    	$allPost = $this->Post->find('all', array(
    		'order' => array('Post.post_id' => 'desc'),
			'conditions' => array('Post.user_id' => $userId)
		));

		// Collect username for admin view
    	$user = $this->Post->User->find('first', array(
    		'conditions' => array('user_id' => $userId)
    	));
    	$username = $user['User']['username'];

        //$allPost = $this->Post->find('all');
		$this->set('posts', $allPost);
		$this->set('username', $username);
		$allCategories = $this->Post->Category->find('list', array('fields' => 'category_name'));
		$this->set('allCategories', $allCategories);
    }

	public function sell() {

		if ($this->request->is('post')) {
			$this->Post->create();
			$this->request->data['Post']['user_id'] = $this->Auth->user('user_id');

			$hasImage = $this->request->data['Photo'][0]['file_path'][0]['name'];
			if (!empty($hasImage)) {
				$photos = $this->request->data['Photo'][0]['file_path'];
				foreach($photos as $k => $v) {
					if(!empty($v['name'])) {
						$file = $v; //put the data into a var for easy use
						$ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
						$arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions

						//only process if the extension is valid
						if(in_array($ext, $arr_ext)) {
							//do the actual uploading of the file. First arg is the tmp name, second arg is
							//where we are putting it
							$createName = time().$file['name'];
							move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/upload/' . $createName);

							//prepare the filename for database entry
							$this->request->data['Photo'][$k]['file_path'] = $createName;
						}
					}
				}
				// Use the following to avoid validation errors:
				unset($this->Post->Photo->validate['post_id']);
				foreach($this->request->data['Photo'] as $v) {
					$imagename = $v['file_path'];
					$this->resizeImage($imagename);
				}
			} else unset($this->request->data['Photo']);

			if ($this->Post->saveAssociated($this->request->data)) {
				// $this->Session->setFlash(__('Your post has been saved.'));
				return $this->redirect(array('controller' => 'users','action' => 'dashboard'));
			}
			$this->Session->setFlash(__('Unable to add your post.'));
			// If save failed, we should remove related images in img/upload/
			foreach($this->request->data['Photo'] as $v) {
				$path = $v['file_path'];
				$file = new File(WWW_ROOT . 'img/upload/'.$path, false, 0777);
				$fileName = $file -> name(); // name without ext
				$ext = $file -> ext(); // file ext

				$large = new File(WWW_ROOT . 'img/upload/'. $fileName . '_L.' . $ext, false, 0777);
				$median = new File(WWW_ROOT . 'img/upload/'. $fileName . '_M.' . $ext, false, 0777);
				$small = new File(WWW_ROOT . 'img/upload/'. $fileName . '_S.' . $ext, false, 0777);

				$file->delete();
				$large->delete();
				$median->delete();
				$small->delete();
			}
		}else{

		$allCategories = $this->Post->Category->find('list', array('fields' => 'category_name'));
		$this->set('allCategories', $allCategories);

		}
	}

    public function buy($id = null) {
    	$currentUser = $this->Auth->user('user_id');
    	$role = $this->Auth->user('role');
    	$allUsers = $this->Post->User->find('list', array(
			'fields' => array('User.username')
		));

    	$this->set('currentUser', $currentUser);
    	$this->set('role', $role);
		$this->set('posts', $this->Post->find('all', array(
			'order' => array('Post.post_id DESC')
			)));
		$this->set('allUsers', $allUsers);
    }

    // $id in message is post_id
    public function message($id = null) {
    	if (!$this->Auth->loggedIn()) {
    		$this->redirect(array('controller' => 'users', 'action' => 'login'));
    	}
    	if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }
        echo $id;
    }

    public function view($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }
        $post = $this->Post->findByPostId($id);
		$imgs = $this->Post->Photo->findAllByPostId($id); // findAllByPostId($id)
        if (!$post) {
            throw new NotFoundException(__('Invalid post'));
        }
        $this->set('post', $post);
		$this->set('imgs', $imgs);
		$allCategories = $this->Post->Category->find('list', array('fields' => 'category_name'));
		$this->set('allCategories', $allCategories);
    }

    public function edit($id = null) {
    	$allCategories = $this->Post->Category->find('list', array('fields' => 'category_name'));
		$this->set('allCategories', $allCategories);
    	if (!$id) {
    		throw new NotFoundException(__('Invalid post'));
    	}
    	$post = $this->Post->findByPostId($id);
    	if (!$post) {
    		throw new NotFoundException(__('Invalid post'));
    	}

    	$imgs = $this->Post->Photo->findAllByPostId($id);
    	$this->set('imgs', $imgs);
    	if ($this->request->is(array('post', 'put'))) {
    		$this->Post->id = $id;
    		$hasImage = $this->request->data['Photo'][0]['file_path'][0]['name'];
    		if (!empty($hasImage)) {
    			$photos = $this->request->data['Photo'][0]['file_path'];
    			foreach($photos as $k => $v) {
    				if(!empty($v['name'])) {
    					$file = $v; //put the data into a var for easy use
						$ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
						$arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions
						//only process if the extension is valid
						if(in_array($ext, $arr_ext)) {
							//do the actual uploading of the file. First arg is the tmp name, second arg is
							//where we are putting it
							$createName = time().$file['name'];
							move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/upload/' . $createName);

							//prepare the filename for database entry
							$this->request->data['Photo'][$k]['file_path'] = $createName;
						}
					}
				}
				// Use the following to avoid validation errors:
				unset($this->Post->Photo->validate['post_id']);
				foreach($this->request->data['Photo'] as $v) {
        			$imagename = $v['file_path'];
					$this->resizeImage($imagename);
				}
			} else unset($this->request->data['Photo']);

        	if ($this->Post->saveAssociated($this->request->data)) {
            	// $this->Session->setFlash(__('Your post has been updated.'));
            	return $this->redirect(array('controller' => 'users', 'action' => 'dashboard'));
        	}
        	$this->Session->setFlash(__('Unable to update your post.'));
        	// If save failed, we should remove related images in img/upload/
			foreach($this->request->data['Photo'] as $v) {
				$path = $v['file_path'];
				$file = new File(WWW_ROOT . 'img/upload/'.$path, false, 0777);
				$fileName = $file -> name(); // name without ext
				$ext = $file -> ext(); // file ext

				$large = new File(WWW_ROOT . 'img/upload/'. $fileName . '_L.' . $ext, false, 0777);
				$median = new File(WWW_ROOT . 'img/upload/'. $fileName . '_M.' . $ext, false, 0777);
				$small = new File(WWW_ROOT . 'img/upload/'. $fileName . '_S.' . $ext, false, 0777);

				$file->delete();
				$large->delete();
				$median->delete();
				$small->delete();
			}
    	}

    	if (!$this->request->data) {
    	    $this->request->data = $post;
    	}
	}

	public function delete($id) {
    	if ($this->request->is('get')) {
    	    throw new MethodNotAllowedException();
    	}

    	if ($this->Post->delete($id)) { //deleteAll(array('Post.post_id' => $id)))
    	    // $this->Session->setFlash(
    	    //     __('The post with id: %s has been deleted.', h($id))
    	    // );
    	    // if ($this->Auth->user('role') == 'admin') {
    	    // 	return $this->redirect($this->referer());
    	    // }
    	    // redirect to dashboard whatever
    	    return $this->redirect(array('controller' => 'users', 'action' => 'dashboard'));
    	}
	}

	// Jack: This function will cost much time if
	// files are large, try to choose/set one size
	// u prefer! And don't forget to change the name
	// in view.ctp if you change this function.
	public function resizeImage($imagename) {
		// increase the memory_limit to handle all images' resize
		ini_set('memory_limit', '256M');
		App::import('Component', 'Image');
		$MyImageCom = new ImageComponent();

		$ext = substr(strrchr($imagename, '.'), 1);
		$name = chop($imagename, '.'.$ext);
		$MyImageCom->prepare("img/upload/".$imagename);
		$MyImageCom->resize(320,200);//width,height,Red,Green,Blue
		$MyImageCom->save("img/upload/".$name.'_L.'.$ext);

		// $MyImageCom->prepare("img/upload/".$imagename);
		// $MyImageCom->resize(92,92);//width,height,Red,Green,Blue
		// $MyImageCom->save("img/upload/".$name.'_M.'.$ext);

		// $MyImageCom->prepare("img/upload/".$imagename);
		// $MyImageCom->resize(103,103);//width,height,Red,Green,Blue
		// $MyImageCom->save("img/upload/".$name.'_S.'.$ext);
	}

	public function search(){

		$available = $this->request->query['search'];
		
		$db = $this->Post->getDataSource();
		
		$result = $db->fetchAll(
					"SELECT p.title, p.post_id, c.category_name, p.price FROM posts p JOIN categories c ON p.category_id=c.category_id 
					WHERE p.title LIKE :query OR c.category_name LIKE :query ", array('query' => "%$available%")
			);

		if (!empty($result['0'])) {
			$post_id = $result['0']['p']['post_id'];
			$imgs = $this->Post->Photo->findAllByPostId($post_id);
			$this->set('searchResult', $result);
			$this->set('imgs', $imgs);
		}

	}

}
?>
