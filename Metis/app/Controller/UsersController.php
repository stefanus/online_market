<?php
App::uses('CakeEmail', 'Network/Email');
class UsersController extends AppController {
  public $helpers = array('Html', 'Form', 'Session');
  public $components = array('Session', 
    'Auth' => array(
      'authenticate' => array(
        'Form' => array(
          'fields' => array('username' => 'email', 'password' => 'password')
        )
      )
    ));

  public function beforeFilter() {
    parent::beforeFilter();
    $this->Auth->allow(array('login', 'register', 'logout', 'resetpassword'));
  }

  public function login() {
    if ($this->request->is('post')) {
      if ($this->Auth->login()) {
        if ($this->Auth->user('suspended') == 'Y') {
          $this->Session->setFlash(__('You have been suspended!'));
          $this->Auth->logout();
          return $this->redirect(array('action' => 'login'));
        }
        return $this->redirect($this->Auth->redirect("/metis/users/dashboard"));
      }
      $this->Session->setFlash(__('Invalid username or password, Please try again'));
    }
  }

  public function logout() {
    $this->Session->setFlash(__("You've logged out successfully!"));
    return $this->redirect($this->Auth->logout());
  }

  public function register() {
    if ($this->request->is('post')) {
      $this->User->create();
      if ($this->User->save($this->request->data)) {
        $this->Session->setFlash(__("You've registered successfully. Please login with your account."));
        return $this->redirect(array('action' => 'login'));

			}
			$this->Session->setFlash(__('Failed to update profile'));
		}
  	}
  	
    public function dashboard() {
    	$role = $this->Auth->user('role');
    	if ($role != 'admin') {
    		$userId = $this->Auth->user('user_id');
    		$posts = $this->User->Post->find('all', array(
    			'conditions' => array('Post.user_id' => $userId),
    			'order' => array('Post.post_id DESC'),
    			'limit' => 8
    		));
    		$this->set('posts', $posts);
    	}
    	$allUsers = $this->User->find('all', array(
			'conditions' => array('User.role != ' => 'admin')
		));
		$this->set('role', $role);
		$this->set('users', $allUsers);

    }
    
  

  // For admin use only, show all users
  public function show() {
    $role = $this->Auth->user('role');
    if ($role != 'admin') {
      return $this->redirect($this->Auth->redirect());
    }
    $allUsers = $this->User->find('all', array(
      'conditions' => array('User.role != ' => 'admin')
    ));
    $this->set('users', $allUsers);
  }

  // For admin use only, delete a user and all his posts/photos
  public function delete($id) {
    if ($this->request->is('get')) {
      throw new MethodNotAllowedException();
    }

    if ($this->User->delete($id)) { //deleteAll(array('Post.post_id' => $id)))
      // $this->Session->setFlash(
      //   __('The post with id: %s has been deleted.', h($id))
      // );
      return $this->redirect(array('action' => 'dashboard'));
    }
  }

  public function update() {
    if ($this->request->is('post')) {
      $this->User->id = CakeSession::read("Auth.User.user_id");

      // If user doesn't enter new password, unset them
      $newpass = $this->request->data['User']['password'];
      $repass = $this->request->data['User']['repass'];
      if (empty($newpass) && empty($repass)) {
        unset($this->request->data['User']['password']);
        unset($this->request->data['User']['repass']);
      }
      
      if ($this->User->save($this->request->data)) {
        $this->Session->write('Auth', $this->User->read(null, $this->Auth->User('id')));
        // $this->Session->setFlash(__("You've updated your profile."));
        return $this->redirect(array('action' => 'dashboard'));
      }
      $this->Session->setFlash(__('Failed to update profile'));
    }
  }

  // if $suspend == 'Y', then we suspend the user
  // if it's 'N' then we get rid of his suspension
  
  public function suspend($id, $suspend) {
    if ($id == null) {
      $this->Session->setFlash(__('Error...'));
      return $this->redirect(array('action' => 'dashboard'));
    }

    $role = $this->Auth->user('role');
    if ($role != 'admin') {

      if ($suspend == 'Y') {
        $this->Session->setFlash(__('Only admin is allowed to suspend users'));
      } else {
        $this->Session->setFlash(__('Only admin is allowed to unsuspend users'));
      }

      // redirect somewhere else
      return $this->redirect(array('action' => 'login'));
    }

    $this->User->id = $id;
    if ($suspend == 'Y') {
      $this->User->set('suspended', 'Y');
    } else {
      $this->User->set('suspended', 'N');
    }
    $this->User->save();

    $user = $this->User->find('first', array('conditions' => array('User.user_id' => $id)));
    $username = $user['User']['username'];

    // if ($suspend == 'Y') {
    //   $this->Session->setFlash(__("$username has been suspended."));
    // } else { 
    //   $this->Session->setFlash(__("$username's suspension has been revoked."));
    // }

    return $this->redirect(array('action' => 'dashboard'));
  }

  // If user forgot his password
  public function resetpassword() {
    if ($this->request->is('post')) {
      $email = $this->request->data['User']['email'];
      $hasUser = $this->User->find('first', array('conditions' => array('User.email' => $email)));
      if ($hasUser) {
        $tmpass = $this->autopassword();
        $hasUser['User']['password'] = $tmpass;
        $username = $hasUser['User']['username'];
        $loginLink = 'http://' . $_SERVER['HTTP_HOST'] . '/metis/users/login';
        $message = "Dear $username: \n\nYour new password is $tmpass, please login and change your password as soon as possible.
        \nLogin via $loginLink.\n\nBest Regards,\nMetis Team";

        if ($this->User->save($hasUser)) {
          $this->send_email($email, $message);
          $this->Session->setFlash(__("New password has been sent to your email. Please check it."));
        }
        // This should not be happen...
        else $this->Session->setFlash(__("Oops... we can't process your request. Please try again or contact us directly."));
      }
      else $this->Session->setFlash(__("This email doesn't exist. Please try again."));
    }
  }

  // Generate a temporary password for user
  public function autopassword($length = 10) {
    $key = '';
    $keys = array_merge(range(0, 9), range('a', 'z'));

    for ($i = 0; $i < $length; $i++) {
      $key .= $keys[array_rand($keys)];
    }
    return $key;
  }

  public function send_email($dest=null, $message = null) {
    $Email = new CakeEmail('gmail');
    $Email->to($dest);
    $Email->subject('Reset password');
    $Email->from ('metis@gmail.com');
    $Email->send($message);
  }

}
?>
