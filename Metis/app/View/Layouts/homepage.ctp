<!DOCTYPE html>
<html lang="en">
  <head>
    <?php echo $this->Html->charset(); ?>
    <?php
    echo $this->Html->meta(array("http-equiv"=>"X-UA-Compatible",
              "content"=>"IE=edge"));
    echo $this->Html->meta(array("name"=>"viewport",
          "content"=>"width=device-width,  initial-scale=1"));
    echo $this->Html->meta(array("name"=>"description",
          "content"=>"this is the description"));
    echo $this->Html->meta(array("name"=>"author",
          "content"=>""))
    ?>
    <link rel="icon" href="../../favicon.ico">

    <title>METIS PHOTOGRAPHY ONLINE MARKET</title>

    <!-- Bootstrap core CSS -->
    <?php
    echo $this->Html->css('bootstrap');
    ?>

    <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <?php
        echo $this->Html->script('bootstrap');
        echo $this->Html->script('carousel');
        echo $this->Html->script('ie-emulation-modes-warning');
    ?>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <?php
    echo $this->Html->css('carousel');
    echo $this->Html->css('responsive-menu-style');
    ?>
  </head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">
        <nav class="navbar navbar-inverse navbar-static-top" role="navigation">
            <?php echo $this->element('header'); ?>
        </nav>
      </div>
    </div>


    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <!-- <ol class="carousel-indicators"> -->
        <!-- <li data-target="#myCarousel" data-slide-to="0" class="active"></li> -->
        <!-- <li data-target="#myCarousel" data-slide-to="1"></li> -->
        <!-- <li data-target="#myCarousel" data-slide-to="2"></li> -->
      <!-- </ol> -->
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <a href="/metis/pages/about"><img src="<?php echo $this->webroot.'img/banner1.jpg' ?>" alt="First slide"></a>
          <div class="container">
          </div>
        </div>
        <div class="item">
          <a href=""><img src="<?php echo $this->webroot.'img/ourmarket.jpg' ?>" alt="Second slide"></a>
          <div class="container">
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class=""></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class=""></span>
        <span class="sr-only">Next</span>
      </a>
    </div><!-- /.carousel -->



    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <div class="row">
        <div class="col-lg-4">
          <img class="img-circle" src="<?php echo $this->webroot.'img/deal1.jpg' ?>" alt="" style="width: 140px; height: 140px;">
          <h2>INSTANT DEALS</h2>
          <p>Check out our latest instant deal!</p>
          <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle" src="<?php echo $this->webroot.'img/deal2.jpg' ?>" alt="" style="width: 140px; height: 140px;">
          <h2>GoPro Accessories</h2>
          <p>Accessorize your GoPro.</p>
          <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle" src="<?php echo $this->webroot.'img/deal3.jpg' ?>" alt="" style="width: 140px; height: 140px;">
          <h2>Latest Posts</h2>
          <p>Check out latest posts from our members.</p>
          <p><a class="btn btn-default" href="/metis/posts/buy" role="button">View details &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->

      <!-- FOOTER -->
      <footer>
        <?php echo $this->element('footer'); ?>
      </footer>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load fuaster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <script src="../../assets/js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
