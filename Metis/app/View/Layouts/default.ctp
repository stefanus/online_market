<!DOCTYPE html>
<html lang="en">
  <head>
    <?php echo $this->Html->charset(); ?>
    <?php
    echo $this->Html->meta(array("http-equiv"=>"X-UA-Compatible",
              "content"=>"IE=edge"));
    echo $this->Html->meta(array("name"=>"viewport",
          "content"=>"width=device-width,  initial-scale=1"));
    echo $this->Html->meta(array("name"=>"description",
          "content"=>"this is the description"));
    echo $this->Html->meta(array("name"=>"author",
          "content"=>""))
    ?>
    <link rel="icon" href="../../favicon.ico">

    <title>METIS PHOTOGRAPHY ONLINE MARKET</title>

    <!-- Bootstrap core CSS -->
    <?php
    echo $this->Html->css('bootstrap');
    ?>

    <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <?php
        echo $this->Html->script('bootstrap');
        echo $this->Html->script('carousel');
        echo $this->Html->script('ie-emulation-modes-warning');
    ?>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <?php
    echo $this->Html->css('carousel');
    ?>
  </head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">
        <nav class="navbar navbar-inverse navbar-static-top" role="navigation">
            <?php echo $this->element('header'); ?>
        </nav>
      </div>
    </div>

    <div class="container marketing">

      <?php echo $this->fetch('content'); ?>

      <!-- FOOTER -->
      <footer>
        <?php echo $this->element('footer'); ?>
      </footer>

    </div><!-- /.container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load fuaster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- <script src="../../dist/js/bootstrap.min.js"></script> -->
    <!-- <script src="../../assets/js/docs.min.js"></script> -->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script> -->
  </body>
</html>
