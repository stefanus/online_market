<!-- File: /app/View/Posts/add.ctp -->

<html>
<head>
    <!-- Bootstrap core CSS -->
    <?php
    echo $this->Html->css('bootstrap');
    ?><!-- Custom styles for this template -->
    <?php
    echo $this->Html->css('signin');
    echo $this->Html->css('search');
    echo $this->Html->css('results');
    echo $this->Html->css('responsive-menu-style');
    ?>

    <title></title>
</head>

<body>
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
              <ul class="nav nav-sidebar">
                <li><a href="/metis/users/dashboard">My Dashboard</a></li>
                <li class="active"><a href="/metis/users/update">Update My Profile</a></li>
                <li><a href="#">My Messages</a></li>
                <li><a href="#">My Posts</a></li>
                <li><a href="#">Settings</a></li>
                <li><a href="/metis/users/logout">Logout</a></li>
              </ul>
            </div>

            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
              <h2 class="page-header">UPDATE MY PROFILE</h2>

                <?php
                    echo $this->Session->flash();
                    echo $this->Form->create('User', array(
                      'inputDefaults' => array(
                        'label' => false
                      ),
                      'class' => 'form-signin',
                      'role' => 'form'
                    ));

                    echo '<p>General: </p>';
                    echo $this->Form->input('first_name', array(
                      'type' => 'text',
                      'class' => 'form-control',
                      'placeholder' => 'First Name',
                      'value' => AuthComponent::user('first_name')
                    ));
                    echo $this->Form->input('last_name', array(
                      'type' => 'text',
                      'class' => 'form-control',
                      'placeholder' => 'Last Name',
                      'value' => AuthComponent::user('last_name')
                    ));

                    echo '<p>Change Password: </p>';

                    echo $this->Form->input('password', array(
                        'type' => 'password',
                        'class' => 'form-control',
                        'placeholder' => 'New Password'
                      ));
                      echo $this->Form->input('repass', array(
                        'type' => 'password',
                        'class' => 'form-control',
                        'placeholder' => 'Confirm New Password'
                      ));

                    echo $this->Form->button('Update', array(
                      'class'=>'btn btn-lg btn-primary btn-block',
                      'type'=>'submit'
                    ));
                    echo $this->Form->end();
                ?>
            </div>
          </div>
        </div>
</body>
</html>
