<!-- File: /app/View/Users/show.ctp -->

<html>
<head>
    <!-- Bootstrap core CSS -->
    <?php
    echo $this->Html->css('bootstrap');
    ?><!-- Custom styles for this template -->
    <?php
    echo $this->Html->css('signin');
    echo $this->Html->css('responsive-menu-style');
    echo $this->Session->flash();
    ?>
    <title></title>
</head>

<h1>All users</h1>

<table>
    <tr>
        <th width='40'>id</th>
        <th width='100'>username</th>
        <th width='100'>first_name</th>
        <th width='100'>last_name</th>
        <th width='150'>email</th>
        <th width='50'>suspended</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($users as $user): ?>
    <tr>
        <td><?php echo $user['User']['user_id']; ?></td>
        <td><?php echo $this->Html->link($user['User']['username'],
            array('controller' => 'posts', 'action' => 'index', $user['User']['user_id'])); ?>
        </td>
        <td><?php echo $user['User']['first_name']; ?></td>
        <td><?php echo $user['User']['last_name']; ?></td>
        <td><?php echo $user['User']['email']; ?></td>
        <td><?php echo $user['User']['suspended']; ?></td>
        <td>
            <?php
    if ($user['User']['suspended'] != 'Y') {
      echo $this->Html->link(
        'Suspend',
        array('action' => 'suspend', $user['User']['user_id'], 'Y')
      );
    }
    else {
      echo $this->Html->link(
        'Unsuspend',
        array('action' => 'suspend', $user['User']['user_id'], 'N')
      );
    }
                echo '&nbsp';
                echo $this->Form->postLink(
                    'Delete',
                    array('action' => 'delete', $user['User']['user_id']),
                    array('confirm' => 'Are you sure?')
                );
            ?>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php unset($user); ?>
</table>
