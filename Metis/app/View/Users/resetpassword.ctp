<html>
<head>
    <!-- Bootstrap core CSS -->
    <?php
    echo $this->Html->css('bootstrap');
    ?>

    <!-- Custom styles for this template -->
    <?php
    echo $this->Html->css('signin');
    echo $this->Html->css('responsive-menu-style');
    ?>
</head>
<table>
	<tr height="500px">
		<td>
        <div class="container">
        <?php
			echo $this->Session->flash();
			echo $this->Form->create('User', array(
				'inputDefaults' => array(
					'label' => false
				),
				'class' => 'form-signin',
				'role' => 'form'
			));
			echo $this->Form->input('email', array(
				'type' => 'email',
				'class' => 'form-control',
				'placeholder' => 'Your Email',
				'required autofocus'
			));

			echo $this->Form->button('Confirm', array(
				'class'=>'btn btn-lg btn-primary btn-block',
				'type'=>'submit'
    		));
    		echo $this->Form->end();
		?>
		</div>
		</td>
	</tr>
</table>



</html>