<!-- File: /app/View/Posts/add.ctp -->

<html>
<head>
    <!-- Bootstrap core CSS -->
    <?php
    echo $this->Html->css('bootstrap');
    ?><!-- Custom styles for this template -->
    <?php
    echo $this->Html->css('signin');
    echo $this->Html->css('search');
    echo $this->Html->css('results');
    echo $this->Html->css('responsive-menu-style');
    ?>

    <title></title>
</head>

<body>
    <!-- <table>
        <tr height="500px">
            <td>
                <div class="container">

                </div><!-- /container -->
            </td>
        </tr>
    </table> -->
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
              <ul class="nav nav-sidebar">
                <li><a href="/metis/users/dashboard">My Dashboard</a></li>
                <li><a href="/metis/users/update">Update My Profile</a></li>
                <li><a href="#">My Messages</a></li>
                <li><a href="#">My Posts</a></li>
                <li><a href="#">Settings</a></li>
                <li><a href="/metis/users/logout">Logout</a></li>
              </ul>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
              <h1 class="page-header">My Posts</h1>

              <div class="table-responsive">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Price</th>
                      </tr>
                    </thead>
                    <tbody>

                      <?php foreach ($posts as $post): ?>
                      <tr>
                          <td><?php echo $post['Post']['post_id']; ?></td>
                          <td>
                              <?php echo $this->Html->link($post['Post']['title'],
                              array('controller' => 'posts', 'action' => 'view', $post['Post']['post_id']));
                              ?>
                          </td>
                          <td><?php echo $post['Post']['price']; ?></td>
                      </tr>
                      <?php endforeach; ?>
                      <?php unset($post); ?>

                    </tbody>
                  </table>
                </div>

            </div>
          </div>
        </div>
</body>
</html>