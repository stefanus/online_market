<!-- File: /app/View/Posts/add.ctp -->

<html>
<head>
    <!-- Bootstrap core CSS -->
    <?php
    echo $this->Html->css('bootstrap');
    ?><!-- Custom styles for this template -->
    <?php
    echo $this->Html->css('signin');
    echo $this->Html->css('search');
    echo $this->Html->css('results');
    echo $this->Html->css('responsive-menu-style');
    ?>

    <title></title>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <br><br>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
        <style>
        .img-zoom {
            width: 100px;
            -webkit-transition: all .2s ease-in-out;
            -moz-transition: all .2s ease-in-out;
            -o-transition: all .2s ease-in-out;
            -ms-transition: all .2s ease-in-out;
        }

        .transition {
            -webkit-transform: scale(1.1);
            -moz-transform: scale(1.1);
            -o-transform: scale(1.1);
            transform: scale(1.1);
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function(){
            $('.img-zoom').hover(function() {
                $(this).addClass('transition');

            }, function() {
                $(this).removeClass('transition');
            });
            $(".slickHoverZoom").slickhover();
          });
    </script>
</head>

<body>
    <!-- <table>
        <tr height="500px">
            <td>
                <div class="container">

                </div><!-- /container -->
            </td>
        </tr>
    </table>
        <div class="container-fluid">
          <div class="row">
            <div class="main">
              <h2 class="page-header">MY DASHBOARD: <?php echo $this->Session->read('Auth.User.username'); ?></h2>

              <div class="row placeholders">
                <div class="col-xs-6 col-sm-3 placeholder">
                  <a href=/metis/users/update>
                  <img src="<?php echo $this->webroot.'img/profile.jpg' ?>" class="img-zoom" alt="Update My Profile">
                  <p font-size="13px"><br><b>UPDATE MY PROFILE</b></p>
                  </a>
                </div>
                <?php if (AuthComponent::user('role') != 'admin'): ?>
                <div class="col-xs-6 col-sm-3 placeholder">
                  <a href=/metis/messages/view>
                  <img src="<?php echo $this->webroot.'img/message.jpg' ?>" class="img-zoom" alt="My Messages">
                  <p font-size="13px"><br><b>MY MESSAGES</b></p>
                </div>
                <div class="col-xs-6 col-sm-3 placeholder">
                  <a href="/metis/posts/index">
                  <img src="<?php echo $this->webroot.'img/posts.jpg' ?>" class="img-zoom" alt="My Posts">
                  <p font-size="13px"><br><b>MY POSTS</b></p>
                  </a>
                </div>
                <div class="col-xs-6 col-sm-3 placeholder">
                  <img src="<?php echo $this->webroot.'img/settings.jpg' ?>" class="img-zoom" alt="Settings">
                  <p font-size="13px"><br><b>SETTINGS</b></p>
                </div>
                <!-- Admin account dashboard just contains 'UPDATE MY PROFILE' & 'ALL USERS' -->
                <!-- Nedd change icon maybe -->
                <?php else: ?>
                <div class="col-xs-6 col-sm-3 placeholder">
                  <a href="/metis/users/dashboard">
                  <img src="<?php echo $this->webroot.'img/posts.jpg' ?>" class="img-zoom" alt="My Posts">
                  <p font-size="13px"><br><b>ALL USERS</b></p>
                  </a>
                </div>
                <?php endif; ?>
              </div>

              
              <h3 class="sub-header">MY PROFILE</h3>
              <?php
                  echo $this->Session->flash();
                  echo $this->Form->create('User', array(
                    'inputDefaults' => array(
                      'label' => false
                    ),
                    'class' => 'form-signin',
                    'role' => 'form'
                  ));

                  echo '<p><b>GENERAL: </b></p>';
                  echo $this->Form->input('first_name', array(
                    'type' => 'text',
                    'class' => 'form-control',
                    'placeholder' => 'First Name',
                    'value' => AuthComponent::user('first_name')
                  ));
                  echo $this->Form->input('last_name', array(
                    'type' => 'text',
                    'class' => 'form-control',
                    'placeholder' => 'Last Name',
                    'value' => AuthComponent::user('last_name')
                  ));

                  echo '<p><b>CHANGE PASSWORD: </b></p>';

                  echo $this->Form->input('password', array(
                      'type' => 'password',
                      'class' => 'form-control',
                      'placeholder' => 'New Password'
                    ));
                    echo $this->Form->input('repass', array(
                      'type' => 'password',
                      'class' => 'form-control',
                      'placeholder' => 'Confirm New Password'
                    ));

                  echo '<div align="right">';
                  echo $this->Form->button('Update', array(
                    'formnovalidate' => true,
                    'class'=>'btn btn-lg btn-primary',
                    'type'=>'submit'
                  ));
                  echo '</div>';
                  echo $this->Form->end();
              ?>
            </div>
          </div>
        </div>
</body>
</html>
