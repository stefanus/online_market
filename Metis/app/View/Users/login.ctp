  <head>
    <!-- Bootstrap core CSS -->
    <?php
    echo $this->Html->css('bootstrap');
    ?>

    <!-- Custom styles for this template -->
    <?php
        echo $this->Html->css('signin');
        echo $this->Html->css('responsive-menu-style');
        ?>
  </head>

  <body>

    <table>
    <tr height="500px">
        <td>
        <div class="container">
        <?php
			echo $this->Session->flash();
			echo $this->Session->flash('auth');
			echo $this->Form->create('User', array(
				'inputDefaults' => array(
					'label' => false
				),
				'class' => 'form-signin',
				'role' => 'form'
			));
			echo $this->Form->input('email', array(
				'type' => 'email',
				'class' => 'form-control',
				'placeholder' => 'Email',
				'required autofocus'
			));
			echo $this->Form->input('password', array(
				'type' => 'password',
				'class' => 'form-control',
				'placeholder' => 'Password',
				'required'
			));
		?>
		<div class="checkbox">
              <label>
                <input type="checkbox" value="remember-me"> Remember me
              </label>
	    </div>
	    <?php
	    echo $this->Form->button('Sign in', array(
	    	'formnovalidate' => true,
			'class'=>'btn btn-lg btn-primary btn-block',
			'type'=>'submit'
		));
		echo $this->Html->link(
			'Forgot your password?',
			array('action' => 'resetpassword'));
		//'<br>';
		echo $this->Form->end();
	    ?>

        </div> <!-- /container -->
        </td>
    </tr>
    </table>

  </body>
