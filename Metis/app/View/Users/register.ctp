  <head>
    <!-- Bootstrap core CSS -->
    <?php
    echo $this->Html->css('bootstrap');
    ?>

    <!-- Custom styles for this template -->
    <?php
        echo $this->Html->css('signin');
        echo $this->Html->css('responsive-menu-style');
        ?>
  </head>

  <body>

    <table>

    <tr height="500px">
        <td>
        <div class="container">
			<?php // generate form for 'users/register'
			echo $this->Session->flash();
			echo $this->Session->flash('auth');
			echo $this->Form->create('User', array(
				    'inputDefaults' => array(
					'label' => false
				),
				'class' => 'form-signin',
				'role' => 'form'
			));
			echo $this->Form->input('username', array(
				'type' => 'text',
				'class' => 'form-control',
				'placeholder' => 'Username',
				'required autofocus'
			));
			echo $this->Form->input('first_name', array(
				'type' => 'text',
				'class' => 'form-control',
				'placeholder' => 'First Name',
				'required'
			));
			echo $this->Form->input('last_name', array(
				'type' => 'text',
				'class' => 'form-control',
				'placeholder' => 'Last Name',
				'required'
			));
			echo $this->Form->input('email', array(
				'type' => 'email',
				'class' => 'form-control',
				'placeholder' => 'Email address',
				'required'
			));
			echo $this->Form->input('password', array(
				'type' => 'password',
				'class' => 'form-control',
				'placeholder' => 'Password',
				'required'
			));
			echo $this->Form->input('repass', array(
				'type' => 'password',
				'class' => 'form-control',
				'placeholder' => 'Confirm Password',
				'required'
			));
			echo $this->Form->hidden('role', array(
				'value' => 'user'
			));

			echo $this->Form->hidden('suspended', array(
				'value' => 'N'
			));

			echo $this->Form->button('Register', array(
				'class'=>'btn btn-lg btn-primary btn-block',
				'type'=>'submit'
			));
			echo $this->Form->end();
			?>

        </div> <!-- /container -->
        </td>
    </tr>
    </table>

  </body>
