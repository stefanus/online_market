<!-- File: /app/View/Messages/view.ctp -->
<html>
<head>
    <!-- Bootstrap core CSS -->
    <?php
    echo $this->Html->css('bootstrap.min');
    ?><!-- Custom styles for this template -->
    <?php
    echo $this->Html->css('signin');
    ?>
    <title></title>
</head>
    <!-- Need change how it looks like. -->
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <br><br>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"> 
        <style> 
        .black_overlay{ 
            display: none; 
            position: absolute; 
            top: 0%; 
            left: 0%; 
            width: 100%; 
            height: 100%; 
            background-color: black; 
            z-index:1001; 
            -moz-opacity: 0.8; 
            opacity:.80; 
            filter: alpha(opacity=88); 
        } 
        .white_content { 
            display: none; 
            position: absolute; 
            top: 25%; 
            left: 25%; 
            width: 55%; 
            height: 55%; 
            padding: 20px; 
            border: 10px solid grey; 
            background-color: white; 
            z-index:1002; 
            overflow: auto; 
        } 
    </style> 

    <body> 
        <h1>
            Messages
        </h1>
        <!-- Display all talks -->
        <?php foreach ($allTalks as $user): ?>

        <p>Message with: <a href = "javascript:void(0)" onclick = "document.getElementById('<?=$user?>').style.display='block';document.getElementById('fade').style.display='block'"><?=$user?></a></p> 
        <div id="<?=$user?>" class="white_content">
        <!-- Show all messages in one talk & reply form in here -->
        <?php foreach ($allKeys as $key){
                if (!empty($sendInfo[$key]) && key($sendInfo[$key]) == $user) {
                    $message = $this->Text->autoLink($sendInfo[$key][$user]);
                    $message = $this->Text->autoParagraph($message);
                    echo $currentUser . ': ' . $message;
                }
                if (!empty($receiveInfo[$key]) && key($receiveInfo[$key]) == $user) {
                    $message = $this->Text->autoLink($receiveInfo[$key][$user]);
                    $message = $this->Text->autoParagraph($message);
                    echo $user . ': ' . $message;
                }
            } 
            echo $this->Form->create('Message', array(
                'action' => 'reply',
                'inputDefaults' => array(
                'label' => false
                ),
                'enctype'=>'multipart/form-data'
            ));
                    
            echo $this->Form->input('message', array(
                'rows' => 5,
                'type' => 'text',
                'placeholder' => 'Message',
                'required'
            ));

            echo $this->Form->hidden('from_user', array(
                'value' => $currentUser
            ));

            echo $this->Form->hidden('to_user', array(
                'value' => $user
            ));
             
            $options = array(
                'label' => 'Reply',
                // 'div' => array(
                //     'class' => 'glass-pill',
                //     )
                );
            echo $this->Form->end($options);
            ?>
        <div><a href = "javascript:void(0)" onclick = "document.getElementById('<?=$user?>').style.display='none';document.getElementById('fade').style.display='none'">Close</a></div>
    </div>
        
        <?php endforeach; ?>
        <div id="fade" class="black_overlay"></div>
    </body>
</html>