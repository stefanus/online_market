<!-- File: /app/View/Posts/add.ctp -->

<html>
<head>
    <!-- Bootstrap core CSS -->
    <?php
    echo $this->Html->css('bootstrap');
    ?><!-- Custom styles for this template -->
    <?php
    echo $this->Html->css('signin');
    echo $this->Html->css('search');
    echo $this->Html->css('results');
    echo $this->Html->css('responsive-menu-style');
    ?>

    <title></title>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <br><br>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
        <style>
        .black_overlay{
            display: none;
            position: absolute;
            top: 0%;
            left: 0%;
            width: 100%;
            height: 100%;
            background-color: black;
            z-index:1001;
            -moz-opacity: 0.8;
            opacity:.80;
            filter: alpha(opacity=88);
        }
        .white_content {
            display: none;
            position: absolute;
            top: 25%;
            left: 25%;
            width: 55%;
            height: 55%;
            padding: 20px;
            border: 10px solid #ffd800;
            background-color: white;
            z-index:1002;
            overflow: auto;
        }
        .img-zoom {
            width: 100px;
            -webkit-transition: all .2s ease-in-out;
            -moz-transition: all .2s ease-in-out;
            -o-transition: all .2s ease-in-out;
            -ms-transition: all .2s ease-in-out;
        }

        .transition {
            -webkit-transform: scale(1.1);
            -moz-transform: scale(1.1);
            -o-transform: scale(1.1);
            transform: scale(1.1);
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function(){
            $('.img-zoom').hover(function() {
                $(this).addClass('transition');

            }, function() {
                $(this).removeClass('transition');
            });
            $(".slickHoverZoom").slickhover();
          });
    </script>

</head>

<body>
    <!-- <table>
        <tr height="500px">
            <td>
                <div class="container">

                </div><!-- /container -->
            </td>
        </tr>
    </table>
        <div class="container-fluid">
          <div class="row">
            <div class="main">
              <h2 class="page-header">MY DASHBOARD: <?php echo $this->Session->read('Auth.User.username'); ?></h2>

              <div class="row placeholders">
                <div class="col-xs-6 col-sm-3 placeholder">
                  <a href=/metis/users/update>
                  <img src="<?php echo $this->webroot.'img/profile.jpg' ?>" class="img-zoom" alt="Update My Profile">
                  <p font-size="13px"><br><b>UPDATE MY PROFILE</b></p>
                  </a>
                </div>
                <div class="col-xs-6 col-sm-3 placeholder">
                  <a href=/metis/messages/view>
                  <img src="<?php echo $this->webroot.'img/message.jpg' ?>" class="img-zoom" alt="My Messages">
                  <p font-size="13px"><br><b>MY MESSAGES</b></p>
                </div>
                <div class="col-xs-6 col-sm-3 placeholder">
                  <a href="/metis/posts/index">
                  <img src="<?php echo $this->webroot.'img/posts.jpg' ?>" class="img-zoom" alt="My Posts">
                  <p font-size="13px"><br><b>MY POSTS</b></p>
                  </a>
                </div>
                <div class="col-xs-6 col-sm-3 placeholder">
                  <img src="<?php echo $this->webroot.'img/settings.jpg' ?>" class="img-zoom" alt="Settings">
                  <p font-size="13px"><br><b>SETTINGS</b></p>
                </div>
              </div>

              
              <h3 class="sub-header">MESSAGES</h3>
                      <!-- Display all talks -->
                      <?php foreach ($allTalks as $user): ?>

                      <p>Message with: <a href = "javascript:void(0)" onclick = "document.getElementById('<?=$user?>').style.display='block';document.getElementById('fade').style.display='block'"><?=$user?></a></p>
                      <div id="<?=$user?>" class="white_content">
                      <!-- Show all messages in one talk & reply form in here -->
                      <?php foreach ($allKeys as $key){
                              if (!empty($sendInfo[$key]) && key($sendInfo[$key]) == $user) {
                                  $message = $this->Text->autoLink($sendInfo[$key][$user]);
                                  $message = $this->Text->autoParagraph($message);
                                  echo '<b>' . $currentUser . ':</b> ' . $message;
                              }
                              if (!empty($receiveInfo[$key]) && key($receiveInfo[$key]) == $user) {
                                  $message = $this->Text->autoLink($receiveInfo[$key][$user]);
                                  $message = $this->Text->autoParagraph($message);
                                  echo '<b>' . $user . ':</b> ' . $message;
                              }
                          }
                          echo $this->Form->create('Message', array(
                              'action' => 'reply',
                              'inputDefaults' => array(
                              'label' => false
                              ),
                              'enctype'=>'multipart/form-data'
                          ));

                          echo $this->Form->input('message', array(
                              'rows' => 5,
                              'type' => 'text',
                              'placeholder' => 'Message',
                              'required'
                          ));

                          echo $this->Form->hidden('from_user', array(
                              'value' => $currentUser
                          ));

                          echo $this->Form->hidden('to_user', array(
                              'value' => $user
                          ));

                          //$options = array(
                          //    'label' => 'Reply',
                          //     'div' => array(
                          //         'class' => 'btn btn-lg btn-primary',
                          //         )
                          //    );

                          echo $this->Form->button('Reply', array(
                              'class'=>'btn btn-lg btn-primary',
                              'type'=>'submit'
                          ));

                          //echo $this->Form->end($options);
                          echo $this->Form->end();
                          ?>
                      <div><a href = "javascript:void(0)" onclick = "document.getElementById('<?=$user?>').style.display='none';document.getElementById('fade').style.display='none'">Close</a></div>
                  </div>

                      <?php endforeach; ?>
                      <div id="fade" class="black_overlay"></div>



            </div>
          </div>
        </div>
</body>
</html>
