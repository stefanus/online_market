<!-- File: /app/View/Posts/add.ctp -->

<html>
<head>
    <!-- Bootstrap core CSS -->
    <?php
    echo $this->Html->css('bootstrap');
    ?><!-- Custom styles for this template -->
    <?php
    echo $this->Html->css('signin');
    echo $this->Html->css('search');
    echo $this->Html->css('results');
    echo $this->Html->css('responsive-menu-style');
    ?>

    <title></title>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <br><br>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
        <style>
        .black_overlay{
            display: none;
            position: absolute;
            top: 0%;
            left: 0%;
            width: 100%;
            height: 100%;
            background-color: black;
            z-index:1001;
            -moz-opacity: 0.8;
            opacity:.80;
            filter: alpha(opacity=88);
        }
        .white_content {
            display: none;
            position: absolute;
            top: 25%;
            left: 25%;
            width: 55%;
            height: 55%;
            padding: 20px;
            border: 10px solid #ffd800;
            background-color: white;
            z-index:1002;
            overflow: auto;
        }
        .img-zoom {
            width: 100px;
            -webkit-transition: all .2s ease-in-out;
            -moz-transition: all .2s ease-in-out;
            -o-transition: all .2s ease-in-out;
            -ms-transition: all .2s ease-in-out;
        }

        .transition {
            -webkit-transform: scale(1.1);
            -moz-transform: scale(1.1);
            -o-transform: scale(1.1);
            transform: scale(1.1);
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function(){
            $('.img-zoom').hover(function() {
                $(this).addClass('transition');

            }, function() {
                $(this).removeClass('transition');
            });
            $(".slickHoverZoom").slickhover();
          });
    </script>

</head>

<body>
    <!-- <table>
        <tr height="500px">
            <td>
                <div class="container">

                </div><!-- /container -->
            </td>
        </tr>
    </table>
        <div class="container-fluid">
          <div class="row">
            <div class="main">
              <h2 class="page-header">CONTACT SELLER: <?php echo $toUser?></h2>

              <?php
              echo $this->Session->flash();
              echo $this->Form->create('Message', array(
              'inputDefaults' => array(
              'label' => false
              ),
              'class' => 'form-signin',
              'role' => 'form',
            'enctype'=>'multipart/form-data'
              ));

              echo $this->Form->input('message', array(
              'rows' => 5,
              'type' => 'text',
              'class' => 'form-control',
              'placeholder' => 'Message',
              'required'
              ));

              echo '<br><p align="right">';
              echo $this->Form->button('Send', array(
              'class'=>'btn btn-lg btn-primary',
              'type'=>'submit'
              ));
              echo $this->Form->end();
              ?>



            </div>
          </div>
        </div>
</body>
</html>
