<div id='cssmenu'>
<ul>
   <a href="/metis"><span><img src="<?php echo $this->webroot.'img/logo.png' ?>"></span></a>
   <li><a href='#'><span>Home</span></a></li>
   <li><a href='#'><span>Products</span></a></li>
   <li><a href='#'><span>Company</span></a></li>
   <li class='last'><a href='#'><span>Contact</span></a></li>
</ul>
</div>
<div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/metis"><img src="<?php echo $this->webroot.'img/logo.png' ?>"></a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li class=""><a href="/metis">Home</a></li>
        <li><a href="/metis/pages/about-us">About</a></li>
	<?php
		$username = $this->Session->read('Auth.User.username');
		if(isset($username)) {
      $role = $this->Session->read('Auth.User.role');
			echo "<li class=dropdown>";
        echo "<a href=# class=dropdown-toggle data-toggle=dropdown>$username<span class=caret></span></a>";
        echo "<ul class=dropdown-menu role=menu>";
        if ($role == 'admin') {
          echo "<li><a href=/metis/users/show>Show all users</a></li>";
          echo "<li><a href=/metis/users/search>Search user</a></li>";
        } else {
          echo "<li><a href=/metis/users/dashboard>My Dashboard</a></li>";
        }
          echo "<li><a href='/metis/users/logout'>Logout</a></li>";
			echo "</ul>";

		}else{
	        echo "<li><a href=/metis/users/login>Login</a></li>";
            echo "<li><a href=/metis/users/register>Register</a></li>";
		}

		?>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Market Place<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="/metis/posts/how-to">How To</a></li>
            <li><a href="/metis/posts/faq">FAQ</a></li>
            <li class="divider"></li>
            <li class="dropdown-header">Trading</li>
            <li><a href="/metis/posts/buy">Buy</a></li>
            <li><a href="/metis/posts/sell">Sell</a></li>
          </ul>
        </li>
    <li><a href="/metis/pages/contact">Contact</a></li>




  </ul>
</div>
</div>