<div id='cssmenu'>
<ul>
   <a href="/metis"><img src="<?php echo $this->webroot.'img/logo.png' ?>"></a>
   <li><a href="/metis"><span>Home</span></a></li>
   <li><a href="/metis/pages/about"><span>About Us</span></a></li>
   <?php
    $username = $this->Session->read('Auth.User.username');
    if(isset($username)) {
     $role = $this->Session->read('Auth.User.role');
        echo "<li class=dropdown>";
        echo "<a href=#>$username<span class=caret></span></a>";
        echo "<ul class=dropdown-menu role=menu>";
        echo "<li><a href=/metis/users/dashboard>My Dashboard</a></li>";
        echo "<li><a href='/metis/users/logout'>Logout</a></li>";
        echo "</ul>";
    }else{
        echo "<li><a href=/metis/users/login>Login</a></li>";
           echo "<li><a href=/metis/users/register>Register</a></li>";
    }
   ?>
   <li class="dropdown">
     <a href="#">Market Place<span class=caret></a>
     <ul class="dropdown-menu" >
        <li><a href="/metis/posts/buy">Buy</a></li>
        <li><a href="/metis/posts/sell">Sell</a></li>
    </ul>
   </li>
   <li class='last'><a href="/metis/pages/contact"><span>Contact</span></a></li>
</ul>
</div>
