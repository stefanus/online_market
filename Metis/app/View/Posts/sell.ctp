<!-- File: /app/View/Posts/sell.ctp -->

<html>
<head>
    <!-- Bootstrap core CSS -->
        <?php
        echo $this->Html->css('bootstrap');
        echo $this->Html->css('style');
        ?><!-- Custom styles for this template -->
        <?php
        echo $this->Html->css('signin');
        echo $this->Html->css('search');
        echo $this->Html->css('results');
        echo $this->Html->css('responsive-menu-style');
        ?>

    <title></title>
</head>

<body>
    <!-- <table>
        <tr height="500px">
            <td>
                <div class="container">

                </div><!-- /container -->
            </td>
        </tr>
    </table> -->
    <div class="container-fluid">
        <h2 class="page-header">MARKET PLACE: SELL</h2>
            <?php
            echo $this->Session->flash();
            echo $this->Form->create('Post', array(
            'inputDefaults' => array(
            'label' => false
            ),
            'class' => 'form-signin',
            'role' => 'form',
            'enctype'=>'multipart/form-data'
            ));
            echo $this->Form->input('title', array(
            'type' => 'text',
            'class' => 'form-control',
            'placeholder' => 'Title',
            'required autofocus'
            ));
            echo $this->Form->input('price', array(
            'type' => 'text',
            'class' => 'form-control',
            'placeholder' => 'Price',
            'required'
            ));
            echo $this->Form->input('post_description', array(
            'rows' => '5',
            'type' => 'text',
            'class' => 'form-control',
            'placeholder' => 'Description',
            'required'
            ));
            ?>


            <div class="checkbox">
            <?php
            echo $this->Form->input('category_id', array(
                'options' => $allCategories,
                'empty' => '(choose category)',
                'class' => 'form-control'));
            echo "Note. multiple photos may be selected in one go";
            echo $this->Form->input('Photo.0.file_path.', array('type' => 'file', 'multiple' => true));
            ?>
            </div>
            <div align="right">
            <?php
            echo $this->Form->button('Sell', array(
            'class'=>'btn btn-lg btn-primary',
            'type'=>'submit'
            ));
            echo '</div>';
            echo $this->Form->end();
            ?>
    </div>
</body>
</html>
