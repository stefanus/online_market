<!-- File: /app/View/Posts/sell.ctp -->

<html>
<head>
    <!-- Bootstrap core CSS -->
        <?php
        echo $this->Html->css('bootstrap');
        echo $this->Html->css('style');
        ?><!-- Custom styles for this template -->
        <?php
        echo $this->Html->css('signin');
        echo $this->Html->css('search');
        echo $this->Html->css('results');
        echo $this->Html->css('responsive-menu-style');
        ?>

    <title></title>
</head>

<body>
    <!-- <table>
        <tr height="500px">
            <td>
                <div class="container">

                </div><!-- /container -->
            </td>
        </tr>
    </table> -->
    <div class="container-fluid">
        <h2 class="page-header">MARKET PLACE: SEARCH</h2>
              <?php

                  echo $this->Form->create('Post', array(
                      'inputDefaults' => array(
                          'label' => false,
                          'div' => false
                      ),
                      'class' => 'form-signin',
                      'role' => 'form',
                      'type' => 'get',
                      'action' => 'search'
                  ));
              ?>
              <table>
                <tr>
                  <td width="600px">

                      <?php echo $this->Form->input('search', array(
                          'type' => 'text',
                          'class' => 'form-control',
                          'placeholder' => 'Search for...',
                          'required'
                      ));
                      ?>
                  </td>
                  <td>
                      <?php echo $this->Form->button('Search', array(
                          'class'=>'btn btn-lg btn-primary',
                          'type'=>'submit'
                      ));


                      ?>
                  </td>
                </tr>
              </table>
              <?php echo $this->Form->end(); ?>


              <h3 class="sub-header">SEARCH RESULTS</h3>
              <table class="table table-striped">
                <thead>
                <tr>

                      <th>#</th>
                      <th>Title</th>
                      <th>Price</th>
                      <th>Category</th>

                    </tr>
                  </thead>
                  <tbody>
                    <?php if (!empty($searchResult)): ?>

                     <?php foreach ($searchResult as $result): ?>
                     <tr>
                        <td> <?php echo $result['p']['post_id']; ?> </td>
                        <td><?php echo $this->Html->link($result['p']['title'],
                            array('controller' => 'posts', 'action' => 'view', $result['p']['post_id']));
                            ?></td>

                        <td> <?php echo $result['p']['price']; ?> </td>
                        <td> <?php echo $result['c']['category_name']; ?> </td>
                    </tr>
                    <?php endforeach; ?>

                  <?php else: ?>
                  <td> No result found </td>
                <?php endif; ?>

                  </tbody>

              </table>


    </div>
</body>
</html>
