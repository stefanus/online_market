<!-- File: /app/View/Posts/index.ctp -->

<html>
<head>
    <!-- Bootstrap core CSS -->
    <?php
    echo $this->Html->css('bootstrap');
    ?><!-- Custom styles for this template -->
    <?php
    echo $this->Html->css('signin');
    echo $this->Html->css('responsive-menu-style');
    echo $this->Session->flash();
    ?>
    <title></title>
</head>

<h1>All posts</h1>
<table>
    <tr>
        <th width='80'>Title</th>
        <th width='80'>Category</th>
        <th width='80'>Price</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($posts as $post): ?>
    <tr>
        <td>
            <?php echo $this->Html->link($post['Post']['title'],
            array('controller' => 'posts', 'action' => 'view', $post['Post']['post_id'])); ?>
        </td>
        <td><?php 
        $categoryId = $post['Post']['category_id'];
        echo h($allCategories[$categoryId]);
        ?></td>
        <td><?php echo $post['Post']['price']; ?></td>
        <td>
            <?php
                echo $this->Form->postLink(
                    'Delete',
                    array('action' => 'delete', $post['Post']['post_id']),
                    array('confirm' => 'Are you sure?')
                );
                echo '&nbsp';
                echo $this->Html->link(
                    'Edit',
                    array('action' => 'edit', $post['Post']['post_id'])
                );
            ?>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php unset($post); ?>
</table>