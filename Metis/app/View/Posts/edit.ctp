<!-- File: /app/View/Posts/sell.ctp -->

<html>
<head>
    <!-- Bootstrap core CSS -->
        <?php
        echo $this->Html->css('bootstrap');
        echo $this->Html->css('style');
        ?><!-- Custom styles for this template -->
        <?php
        echo $this->Html->css('signin');
        echo $this->Html->css('search');
        echo $this->Html->css('results');
        echo $this->Html->css('responsive-menu-style');
        ?>

    <title></title>
</head>

<body>
    <!-- <table>
        <tr height="500px">
            <td>
                <div class="container">

                </div><!-- /container -->
            </td>
        </tr>
    </table> -->
    <div class="container-fluid">
        <h2 class="page-header">EDIT POST</h2>
        <?php
        echo $this->Session->flash();
        echo $this->Form->create('Post', array(
        'inputDefaults' => array(
        'label' => false
        ),
        'class' => 'form-signin',
        'role' => 'form',
        'enctype'=>'multipart/form-data'
        ));
        echo $this->Form->input('title', array(
        'type' => 'text',
        'class' => 'form-control',
        'placeholder' => 'Title',
        'required autofocus'
        ));
        echo $this->Form->input('price', array(
        'type' => 'text',
        'class' => 'form-control',
        'placeholder' => 'Price',
        'required'
        ));
        echo $this->Form->input('post_description', array(
        'rows' => '5',
        'type' => 'text',
        'class' => 'form-control',
        'placeholder' => 'Description',
        'required'
        ));
        echo $this->Form->input('post_id', array('type' => 'hidden'));
        echo '<br>';
        foreach($imgs as $v) {
            $path = $v['Photo']['file_path'];
            $ext = substr(strrchr($path, '.'), 1);
            $name = chop($path, '.' . $ext);
            $displayPath = $name . '_L.' . $ext;

            // 3rd param array is used to specify html attributes
            //echo "This is your current photo for the listing!<br>";
            echo $this->Html->link(
                $this->Html->image("upload/$displayPath", array('hspace' => '5')), "/img/upload/$path", array(
                'target' => '_blank',
                'escape' => false
            ));
        }

        ?>

        <div class="checkbox">
            <?php
            echo $this->Form->input('category_id', array(
            'options' => $allCategories,
            'empty' => '(choose category)',
            'class' => 'form-control'
            ));
            echo $this->Form->input('Photo.0.file_path.', array(
            'type' => 'file',
            'multiple' => true,
            ));
            ?>
        </div>
        <p align = "right">
            <?php
            echo $this->Form->button('Save changes', array(
            'class'=>'btn btn-lg btn-primary',
            'type'=>'submit'
            ));
            echo $this->Form->end();
            ?>
        </p>
    </div>
</body>
</html>
