<!-- File: /app/View/Posts/view.ctp -->

<html>
<head>
    <!-- Bootstrap core CSS -->
    <?php
    echo $this->Html->css('bootstrap');
    ?><!-- Custom styles for this template -->
    <?php
    echo $this->Html->css('signin');
    echo $this->Html->css('responsive-menu-style');
    echo $this->Html->css('js-image-slider');
    echo $this->Html->script('js-image-slider');
	echo $this->Session->flash();
    ?>
    <title></title>

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <br><br>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
        <style>
        .img-zoom {
            width: 250px;
            -webkit-transition: all .2s ease-in-out;
            -moz-transition: all .2s ease-in-out;
            -o-transition: all .2s ease-in-out;
            -ms-transition: all .2s ease-in-out;
        }

        .transition {
            -webkit-transform: scale(1.2);
            -moz-transform: scale(1.2);
            -o-transform: scale(1.2);
            transform: scale(1.2);
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function(){
            $('.img-zoom').hover(function() {
                $(this).addClass('transition');

            }, function() {
                $(this).removeClass('transition');
            });
            $(".slickHoverZoom").slickhover();
          });
    </script>

</head>
<body>

    <!-- <table>
        <tr height="500px">
            <td>
                <div class="container">

                </div><!-- /container -->
            </td>
        </tr>
    </table>
    <div class="container-fluid">
        <h2 class="page-header"><?php echo h($post['Post']['title']);?></h2>

        <p><b>DESCRIPTION:</b> <?php echo $post['Post']['post_description']; ?></p>

        <p><b>CATEGORY:</b> <?php
            $categoryId = $post['Post']['category_id'];
            echo h($allCategories[$categoryId]);
            ?></p>

        <p><b>PRICE:</b> <?php echo h($post['Post']['price']); ?></p>

        <p><b>SELLER:</b> <?php echo h($post['User']['username']); ?></p>

        <center>
        <p><?php
            foreach($imgs as $v) {
                $path = $v['Photo']['file_path'];
                $ext = substr(strrchr($path, '.'), 1);
                $name = chop($path, '.' . $ext);
                //$displayPath = $name . '_L.' . $ext;
                $displayPath = $name . '.' . $ext;

                // 3rd param array is used to specify html attributes
                echo $this->Html->link(
                    $this->Html->image("upload/$displayPath", array('class' => 'img-zoom', 'hspace' => '20')), "/img/upload/$path", array(
                    'target' => '_blank',
                    'escape' => false
                ));
            }
        ?>
        </p>
        </center>

        <p align="right"><?php
            if (AuthComponent::user('user_id') == $post['Post']['user_id']) {
                echo $this->Form->postLink(
                    'Delete',
                    array('action' => 'delete', $post['Post']['post_id']),
                    array('confirm' => 'Are you sure?'));
                echo '&nbsp';
                echo $this->Html->link(
                    'Edit',
                    array('action' => 'edit', $post['Post']['post_id']));
            } else {
                echo $this->Html->link(
                    'Buy',
                    array('controller' => 'messages', 'action' => 'add', $post['Post']['post_id']));
            }

        ?></p>

    </div>


</body>

</html>
