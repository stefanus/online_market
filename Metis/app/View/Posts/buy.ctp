<!-- File: /app/View/Posts/add.ctp -->

<html>
<head>
    <!-- Bootstrap core CSS -->
    <?php
    echo $this->Html->css('bootstrap');
    echo $this->Html->css('style');
    ?><!-- Custom styles for this template -->
    <?php
    echo $this->Html->css('search');
    echo $this->Html->css('results');
    echo $this->Html->css('responsive-menu-style');
    ?>

    <title></title>
</head>

<body>
            <!-- <table>
                    <tr height="500px">
                        <td>
                            <div class="container">

                            </div><!-- /container -->
                        </td>
                    </tr>
                </table> -->
                <div class="container-fluid">
              <h2 class="page-header">MARKET PLACE: BUY</h2>
              <?php
                  echo $this->Session->flash('auth');
                  echo $this->Form->create('Post', array(
                      'inputDefaults' => array(
                          'label' => false,
                          'div' => false
                      ),
                      'class' => 'form-signin',
                      'role' => 'form',
                      'type' => 'get',
                      'action' => 'search'
                  ));
              ?>
              <table>
                <tr>
                  <td width="600px">
                      <?php echo $this->Form->input('search', array(
                          'type' => 'text',
                          'class' => 'form-control',
                          'placeholder' => 'Search for...',
                          'required'
                      ));
                      ?>
                  </td>
                  <td>
                      <?php echo $this->Form->button('Search', array(
                          'class'=>'btn btn-lg btn-primary',
                          'type'=>'submit'
                      ));
                      ?>
                  </td>
                </tr>
              </table>
              <?php echo $this->Form->end(); ?>

              <h3 class="sub-header">FEATURED PRODUCTS</h3>

              <div class="row placeholders">
                <div class="col-xs-6 col-sm-3 placeholder">
                  <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="thumbnail">
                  <h4>Label</h4>
                  <span class="text-muted">Something else</span>
                </div>
                <div class="col-xs-6 col-sm-3 placeholder">
                  <img data-src="holder.js/200x200/auto/vine" class="img-responsive" alt="thumbnail">
                  <h4>Label</h4>
                  <span class="text-muted">Something else</span>
                </div>
                <div class="col-xs-6 col-sm-3 placeholder">
                  <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="thumbnail">
                  <h4>Label</h4>
                  <span class="text-muted">Something else</span>
                </div>
                <div class="col-xs-6 col-sm-3 placeholder">
                  <img data-src="holder.js/200x200/auto/vine" class="img-responsive" alt="thumbnail">
                  <h4>Label</h4>
                  <span class="text-muted">Something else</span>
                </div>
              </div>

              <h3 class="sub-header">SEARCH RESULTS</h3>
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Title</th>
                      <th>Seller</th>
                      <th>Price</th>
                    </tr>
                  </thead>
                  <tbody>

                    <?php foreach ($posts as $post): ?>
                    <tr>
                        <td><?php echo $post['Post']['post_id']; ?></td>
                        <td>
                            <?php echo $this->Html->link($post['Post']['title'],
                            array('controller' => 'posts', 'action' => 'view', $post['Post']['post_id']));
                            ?>
                        </td>
                        <td><?php 
                        $sellerId = $post['Post']['user_id'];
                        echo $allUsers[$sellerId]; 
                        ?></td>
                        <td><?php echo $post['Post']['price']; ?></td>
                        <td>
                          <?php 
                          // Buyers can only buy other users' gear. 
                          // Admin can view all users' gear
                          if ($currentUser != $post['Post']['user_id']
                          &&  $role != 'admin' ) {
                            // can add 3rd parameter like this to make it look like a button: 
                            // array('class' => 'btn btn-lg btn-primary')
                            echo $this->Html->link(
                              'Buy',
                              array('controller' => 'messages', 'action' => 'add', $post['Post']['post_id'])
                            ); 
                          } else {
                            echo $this->Html->link(
                              'View',
                              array('controller' => 'posts', 'action' => 'view', $post['Post']['post_id'])
                            );
                          }
                          ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                    <?php unset($post); ?>

                  </tbody>
                </table>
              </div>
            </div>

        
    
       
</body>
</html>
