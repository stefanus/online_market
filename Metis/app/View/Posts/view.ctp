<!-- File: /app/View/Posts/view.ctp -->

<html>
<head>
    <!-- Bootstrap core CSS -->
    <?php
    echo $this->Html->css('bootstrap');
    ?><!-- Custom styles for this template -->
    <?php
    echo $this->Html->css('signin');
    echo $this->Html->css('responsive-menu-style');
    echo $this->Html->css('js-image-slider');
    echo $this->Html->script('js-image-slider');
	echo $this->Session->flash();
    ?>
    <title></title>

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <br><br>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
        <style>
        ul.enlarge{
        list-style-type:none; /*remove the bullet point*/
        margin-left:0;
        }
        ul.enlarge li{
        display:inline-block; /*places the images in a line*/
        position: relative;
        z-index: 0; /*resets the stack order of the list items - later we'll increase this*/
        margin:10px 40px 0 20px;
        }
        ul.enlarge img{
        background-color:#ffd800;
        padding: 6px;
        -webkit-box-shadow: 0 0 6px rgba(132, 132, 132, .75);
        -moz-box-shadow: 0 0 6px rgba(132, 132, 132, .75);
        box-shadow: 0 0 6px rgba(132, 132, 132, .75);
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        border-radius: 0px;
        }
        ul.enlarge span{
        position:absolute;
        left: -9999px;
        background-color:#ffd800;
        padding: 10px;
        font-family: 'Droid Sans', sans-serif;
        font-size:.9em;
        text-align: center;
        color: #495a62;
        -webkit-box-shadow: 0 0 20px rgba(0,0,0, .75));
        -moz-box-shadow: 0 0 20px rgba(0,0,0, .75);
        box-shadow: 0 0 20px rgba(0,0,0, .75);
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        border-radius:0px;
        }
        ul.enlarge li:hover{
        z-index: 50;
        cursor:pointer;
        }
        ul.enlarge span img{
        padding:2px;
        background:#ccc;
        }
        ul.enlarge li:hover span{
        top: -300px; /*the distance from the bottom of the thumbnail to the top of the popup image*/
        left: -20px; /*distance from the left of the thumbnail to the left of the popup image*/
        }
        ul.enlarge li:hover:nth-child(2) span{
        left: -100px;
        }
        ul.enlarge li:hover:nth-child(3) span{
        left: -200px;
        }
        /**IE Hacks - see http://css3pie.com/ for more info on how to use CS3Pie and to download the latest version**/
        ul.enlarge img, ul.enlarge span{
        behavior: url(pie/PIE.htc);
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function(){
            $('.img-zoom').hover(function() {
                $(this).addClass('transition');

            }, function() {
                $(this).removeClass('transition');
            });
            $(".slickHoverZoom").slickhover();
          });
    </script>

</head>
<body>

    <!-- <table>
        <tr height="500px">
            <td>
                <div class="container">

                </div><!-- /container -->
            </td>
        </tr>
    </table>
    <div class="container-fluid">
        <h2 class="page-header"><?php echo h($post['Post']['title']);?></h2>

        <p><b>DESCRIPTION:</b> <?php echo $post['Post']['post_description']; ?></p>

        <p><b>CATEGORY:</b> <?php
            $categoryId = $post['Post']['category_id'];
            echo h($allCategories[$categoryId]);
            ?></p>

        <p><b>PRICE:</b> <?php echo h($post['Post']['price']); ?></p>

        <p><b>SELLER:</b> <?php echo h($post['User']['username']); ?></p>

        <center>
        <ul class="enlarge">
        <?php
            foreach($imgs as $v) {
                $path = $v['Photo']['file_path'];
                $ext = substr(strrchr($path, '.'), 1);
                $name = chop($path, '.' . $ext);
                //$displayPath = $name . '_L.' . $ext;
                $displayPath = $name . '.' . $ext;

                // 3rd param array is used to specify html attributes
                echo '<li>';
                echo $this->Html->link(
                    $this->Html->image("upload/$displayPath", array('width' => '150')), "/img/upload/$path", array(
                    'target' => '_blank',
                    'escape' => false
                ));
                echo '<span>';
                echo $this->Html->link(
                    $this->Html->image("upload/$displayPath", array('width' => '500')), "/img/upload/$path", array(
                    'target' => '_blank',
                    'escape' => false
                ));
                echo '<br /></span>';
                echo '</li>';
            }
        ?>
        </ul>
        </center>

        <p align="right"><?php
            if (AuthComponent::user('user_id') == $post['Post']['user_id']
             || AuthComponent::user('role') == 'admin') {
                echo $this->Form->postLink(
                    'Delete',
                    array('action' => 'delete', $post['Post']['post_id']),
                    array('confirm' => 'Are you sure?'));
                echo '&nbsp';
                echo $this->Html->link(
                    'Edit',
                    array('action' => 'edit', $post['Post']['post_id']));
            } else {
                echo $this->Html->link(
                    'Buy',
                    array('controller' => 'messages', 'action' => 'add', $post['Post']['post_id']));
            }

        ?></p>

    </div>


</body>

</html>
