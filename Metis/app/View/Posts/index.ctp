<!-- File: /app/View/Posts/add.ctp -->

<html>
<head>
    <!-- Bootstrap core CSS -->
    <?php
    echo $this->Html->css('bootstrap');
    ?><!-- Custom styles for this template -->
    <?php
    echo $this->Html->css('search');
    echo $this->Html->css('results');
    echo $this->Html->css('responsive-menu-style');
    ?>

    <title></title>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <br><br>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
        <style>
        .img-zoom {
            width: 100px;
            -webkit-transition: all .2s ease-in-out;
            -moz-transition: all .2s ease-in-out;
            -o-transition: all .2s ease-in-out;
            -ms-transition: all .2s ease-in-out;
        }

        .transition {
            -webkit-transform: scale(1.1);
            -moz-transform: scale(1.1);
            -o-transform: scale(1.1);
            transform: scale(1.1);
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function(){
            $('.img-zoom').hover(function() {
                $(this).addClass('transition');

            }, function() {
                $(this).removeClass('transition');
            });
            $(".slickHoverZoom").slickhover();
          });
    </script>
</head>

<body>
    <!-- <table>
        <tr height="500px">
            <td>
                <div class="container">

                </div><!-- /container -->
            </td>
        </tr>
    </table>
        <div class="container-fluid">
          <div class="row">
            <!-- <div class="col-sm-3 col-md-2 sidebar">
              <ul class="nav nav-sidebar">
                <li><a href="#">Popular</a></li>
                <li><a href="#">Recently Added</a></li>
                <li><a href="#">Sale</a></li>
              </ul>
              <ul class="nav nav-sidebar">
                <li><a href="">Digital SLR Cameras</a></li>
                <li><a href="">Compact Cameras</a></li>
                <li><a href="">Video Cameras</a></li>
                <li><a href="">Camera Lenses</a></li>
                <li><a href="">Tripods and Monopods</a></li>
                <li><a href="">Camera Bags</a></li>
                <li><a href="">Studio Equipment</a></li>
              </ul>
            </div> -->
            <div class="main">
              <h2 class="page-header">MY DASHBOARD: <?php echo $this->Session->read('Auth.User.username'); ?></h2>

              <div class="row placeholders">
                <div class="col-xs-6 col-sm-3 placeholder">
                  <a href=/metis/users/update>
                  <img src="<?php echo $this->webroot.'img/profile.jpg' ?>" class="img-zoom" alt="Update My Profile">
                  <p font-size="13px"><br><b>UPDATE MY PROFILE</b></p>
                  </a>
                </div>
                <?php if (AuthComponent::user('role') != 'admin'): ?>
                <div class="col-xs-6 col-sm-3 placeholder">
                  <a href=/metis/messages/view>
                  <img src="<?php echo $this->webroot.'img/message.jpg' ?>" class="img-zoom" alt="My Messages">
                  <p font-size="13px"><br><b>MY MESSAGES</b></p>
                </div>
                <div class="col-xs-6 col-sm-3 placeholder">
                  <a href="/metis/posts/index">
                  <img src="<?php echo $this->webroot.'img/posts.jpg' ?>" class="img-zoom" alt="My Posts">
                  <p font-size="13px"><br><b>MY POSTS</b></p>
                  </a>
                </div>
                <div class="col-xs-6 col-sm-3 placeholder">
                  <img src="<?php echo $this->webroot.'img/settings.jpg' ?>" class="img-zoom" alt="Settings">
                  <p font-size="13px"><br><b>SETTINGS</b></p>
                </div>
                <!-- Admin account dashboard just contains 'UPDATE MY PROFILE' & 'ALL USERS' -->
                <!-- Nedd change icon maybe -->
                <?php else: ?>
                <div class="col-xs-6 col-sm-3 placeholder">
                  <a href="/metis/users/dashboard">
                  <img src="<?php echo $this->webroot.'img/posts.jpg' ?>" class="img-zoom" alt="My Posts">
                  <p font-size="13px"><br><b>ALL USERS</b></p>
                  </a>
                </div>
                <?php endif; ?>
              </div>

              
              <h3 class="sub-header"><?php if (AuthComponent::user('role') != 'admin') {
                echo "MY POSTS";
              } else echo "$username's posts";
              ?></h3>
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      
                      <th>#</th>
                      <th>Title</th>
                      <th>Price</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                    
                      <?php foreach ($posts as $post): ?>
                      <tr>
                        <td><?php echo $post['Post']['post_id']; ?></td>
                        <td>
                            <?php echo $this->Html->link($post['Post']['title'],
                            array('controller' => 'posts', 'action' => 'view', $post['Post']['post_id']));
                            ?>
                        </td>
                        <td><?php echo $post['Post']['price']; ?></td>
                        <td>
                          <?php
                          echo $this->Form->postLink(
                            'Delete',
                            array('controller' => 'posts', 'action' => 'delete', $post['Post']['post_id']),
                            array('confirm' => 'Are you sure?')
                            );
                          echo '</td><td>';
                          echo $this->Html->link(
                            'Edit',
                            array('controller' => 'posts', 'action' => 'edit', $post['Post']['post_id']));
                            ?>
                          </td>
                      </tr>
                      <?php endforeach; ?>
                      <?php unset($post); ?>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
</body>
</html>
