<?php
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {
	public $hasMany = 'Post'; 
	public $primaryKey = 'user_id';
	public $relatedImages;

	public $validate = array(
		'username' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'A username is required'
			),
			'unique' => array(
				'rule' => 'isUnique',
				'message' => 'This username has already been taken.'
			)
		),
		'first_name' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'A first name is required'
			)
		),
		'last_name' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'A last name is required'
			)
		),
		'email' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'An email is required',
				'last' => true
			),
			'is_valid' => array(
				'rule' => array('email', true),
				'message' => 'A valid email is required'
			),
			'unique' => array(
				'rule' => 'isUnique',
				'message' => 'This email has already been taken.'
      ),
		),
		'password' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'A password is required'
			),
			'length' => array(
				'rule' => array('between', 8, 40),
				'message' => 'Your password must be between 8 and 40 characters.'
			),
		),
		'repass' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please confirm your password'
			),
			'length' => array(
				'rule' => array('between', 8, 40),
				'message' => 'Your password must be between 8 and 40 characters.'
			),
			'compare' => array(
				'rule' => array('validate_passwords'),
				'message' => 'The passwords you entered do not match.'
			)
		)
	);
	
	public function beforeSave($options = array()) {
	    if (isset($this->data[$this->alias]['password'])) {
	        $passwordHasher = new BlowfishPasswordHasher();
	        $this->data[$this->alias]['password'] = $passwordHasher->hash(
	            $this->data[$this->alias]['password']
	        );
	    }
	    return true;
	}
	
	function validate_passwords() {
		return $this->data[$this->alias]['password'] === $this->data[$this->alias]['repass'];
	}

	// Delete all photos stored in img/upload/
	public function beforeDelete($cascade = true) {
		$allPost = $this->Post->find('all', array('conditions' => array('Post.user_id' => $this->id)));
		foreach ($allPost as $value) {
			$photos = $this->Post->Photo->find('list', 
				array(
					'fields' => array('Photo.file_path'),
					'conditions' => array('Photo.post_id' => $value['Post']['post_id'])
					));
			if (empty($GLOBALS['relatedImages'])) {
				$GLOBALS['relatedImages'] = $photos;
			} else {
				$GLOBALS['relatedImages'] = array_merge($GLOBALS['relatedImages'], $photos);
			}
		}
  		return true;
	}

	public function afterDelete() {
		if (!empty($GLOBALS['relatedImages'])) {
			$relatedImages = $GLOBALS['relatedImages'];
			foreach($relatedImages as $path) {
				$file = new File(WWW_ROOT . 'img/upload/'.$path, false, 0777);
				$fileName = $file->name(); // name without ext
				$ext = $file->ext(); // file ext
				
				$large = new File(WWW_ROOT . 'img/upload/'. $fileName . '_L.' . $ext, false, 0777);
				$median = new File(WWW_ROOT . 'img/upload/'. $fileName . '_M.' . $ext, false, 0777);
				$small = new File(WWW_ROOT . 'img/upload/'. $fileName . '_S.' . $ext, false, 0777);
				
				$file->delete();
				$large->delete();
				$median->delete();
				$small->delete();
			}
		}
	}
}
?>
