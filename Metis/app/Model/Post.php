<?php
App::uses('File', 'Utility');

class Post extends AppModel {
	public $hasMany = array('Photo');
	public $primaryKey = 'post_id';
	public $belongsTo = array('User'); // 'Category',
	public $relatedImages;
	public $hasOne = array('Category'=> array('foreignKey' => 'category_id'));

	public $validate = array(
		'title' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'Title is required'
			),
		),
		'price' => array(
			'required' => array(
				'rule' => array('notEmpty', 'numeric'),
				'message' => 'Price is required'
			)
		),
		'post_description' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'Description is required'
			)
		),
		'category_category_id' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please choose one category'
			)
      	)
	);

	// Delete all photos stored in img/upload/
	public function beforeDelete($cascade = true) {
		$GLOBALS['relatedImages'] = $this->Photo->find('all', array('conditions' => array('Photo.post_id' => $this->id)));
  		return true;
	}

	public function afterDelete() {
		$relatedImages = $GLOBALS['relatedImages'];
		foreach($relatedImages as $v) {
			$path = $v['Photo']['file_path'];
			$file = new File(WWW_ROOT . 'img/upload/'.$path, false, 0777);
			$fileName = $file -> name(); // name without ext
			$ext = $file -> ext(); // file ext

			$large = new File(WWW_ROOT . 'img/upload/'. $fileName . '_L.' . $ext, false, 0777);
			$median = new File(WWW_ROOT . 'img/upload/'. $fileName . '_M.' . $ext, false, 0777);
			$small = new File(WWW_ROOT . 'img/upload/'. $fileName . '_S.' . $ext, false, 0777);

			$file->delete();
			$large->delete();
			$median->delete();
			$small->delete();
		}
	}
}

?>
