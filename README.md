# README #

### What is this repository for? ###

* Quick summary
* Version

### How do I get set up? ###

* I am using PHPStorm, XAMPP and SQLyog on Windows, so if you guys want to use it too install them and read this: https://docs.google.com/document/d/1Jobw84r0wO-6A-6M_Mac3pTnZQz2cvC2TGofOUluD5c/edit?pli=1

#### XAMPP configuration ####
* Add another virtual host to the *httpd-vhosts.conf* file (there's a commented example in the file already), change the Document Root
* The Document root/webroot is online_market/Metis/app/webroot
* Add another localhost name resolution for the project to the *hosts* file e.g. "127.0.0.1       local.metis.com"
* **Note: to edit these two files you need to be an admin**
* Restart XAMPP

#### Database configuration ####
* In SQLyog just create a new connection name it "localhost", leave all settings as it is (i.e. username = root, password is blank).
* After you're connected you will see all the database you have in your local host

### Contribution guidelines ###

* When implementing, create a new branch, don't implement and push to the master branch.
* Git cheat sheet: https://www.atlassian.com/dms/wac/images/landing/git/atlassian_git_cheatsheet.pdf
* Writing tests
* Code review
* Other guidelines